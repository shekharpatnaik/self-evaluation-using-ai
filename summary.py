from anthropic import Anthropic, HUMAN_PROMPT, AI_PROMPT
import streamlit as st
from concurrent.futures import ThreadPoolExecutor, as_completed
from datetime import datetime

MAX_WORKERS = 6

def summarize_achievements(list_of_mrs, temperature, projects, progress_callback):

    # print("Calculating batches")
    anthropic = Anthropic()
    batches = []
    current_text = ""
    for mr in list_of_mrs:

        project = projects.get(mr.project_id, "")
        project_name = project.name if project else ""
        created_at = datetime.strptime(mr.created_at, '%Y-%m-%dT%H:%M:%S.%fZ')
        merged_at = datetime.strptime(mr.merged_at, '%Y-%m-%dT%H:%M:%S.%fZ')
        merged_in_days = (merged_at - created_at).days + 1

        text = f"""
        Title: {mr.title}
        Project: {project_name}
        Description: {mr.description}
        Link (use these in the citations): {mr.web_url}
        Merged in Days: {merged_in_days}
        No of discussions: {mr.user_notes_count}
        Changed Lines: {mr.count_of_changed_lines}
        ----
        """
        if len(current_text) + len(text) > 100000:
            batches.append(current_text)
            current_text = text
        else:
            current_text += f"\n{text}"

    batches.append(current_text)

    # print("Finished Calculating batches")

    batch_result = []
    no_of_batches = len(batches)
    percentage_per_batch = 90 / no_of_batches
    progress = 0

    with ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
        futures = []
        for batch in batches:
            futures.append(executor.submit(process_batch, anthropic, batch, temperature))

        for future in as_completed(futures):
            batch_result.append(future.result())
            progress += percentage_per_batch
            progress_callback(int(progress))

    batch_result = "\n".join(batch_result)

    total_word_count = min(1500, len(list_of_mrs)*50)

    achievements_prompt = f"""
    Your task is to generate a summary of achievements of an engineer based on the work he or she has done this year. You are given 
    summaries of the merge requests that they've worked on. Can you generate an achievements report based on this data and respond in a well formated markdown?
    **Very important**: Please use information from the MRs provided below only to generate the summary. Do not use any other facts to generate the summary.

    Please note:
    - The markdown should have the following sections Introduction, Key Achievements, and Conclusion. 
    - Please try to divide the Key Achievement section into sub sections.
    - Please note that your summary should be around {total_word_count} words. The citations do not count towards the work limits.
    - Please provide more focus for the ones where the 'merged in days' are higher. This means that the engineer spent more time on the MR.
    - Don't use actual people names, use pronouns of "They" or refer to the person as a "team member"
    - MRs where they have been a lot of discussions are also more important as it would need a lot of back and fourth and multiple people contributing.
    - When summarising the achievements, please provide a link to the related MRs that were used for the summarization as a citation eg. The team member has shown deep technical expertise in security ([1](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/merge_requests/123), [2](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/merge_requests/5)).
    - If the number of changed lines is not 0, then give more importance to those MRs with more changes as the team member probably spent a lot of time on these.   

    Merge Request Summary:
    ----------------------
    ```
    {batch_result}
    ```
    """
    print("Final prompt:")
    print("------------")
    print(achievements_prompt)
    print("------------")
    completion = anthropic.completions.create(
            temperature=temperature,
            model="claude-2",
            max_tokens_to_sample=2048,
            prompt=f"{HUMAN_PROMPT} {achievements_prompt}{AI_PROMPT}",
        )

    st.markdown(completion.completion)
    progress_callback(100)

def process_batch(anthropic, batch, temperature):
    prompt = f"""
        You are given a list of merge requests and their descriptions. The descriptions are verbose and need to be summarized to
        highlight the essence of the merge request. Please provide a larger description for when the merged in days are higher.

        Here is the list of MRs:

        {batch}

        Please summarize the merge requests in the following format:

        Title: <title>
        Project: <project>
        Summary: <summary>
        Link: <link>
        Merged in Days: <Merged in days>
        No of discussions: <No of discussions>
        Changed Lines: <No of lines changed in MR>
        ---

        """

    # print(f"prompt is {prompt}")
    completion = anthropic.completions.create(
        temperature=temperature,
        model="claude-2",
        max_tokens_to_sample=1024,
        prompt=f"{HUMAN_PROMPT} {prompt}{AI_PROMPT}",
    )

    return completion.completion
    