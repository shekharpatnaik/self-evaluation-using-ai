import pandas as pd
from datetime import datetime

from merge_requests import get_changed_lines_for_mr

def create_mr_df(mrs, projects):
    rows = []
    for mr in mrs:
        milestone = ""
        if mr.milestone:
            milestone = mr.milestone["title"]
        
        project = projects[mr.project_id].name
        created_at = datetime.strptime(mr.created_at, '%Y-%m-%dT%H:%M:%S.%fZ')
        merged_at = datetime.strptime(mr.merged_at, '%Y-%m-%dT%H:%M:%S.%fZ')
        merged_in_days = (merged_at - created_at).days + 1

        rows.append([mr.title, project, milestone, mr.created_at, mr.merged_at, merged_in_days, mr.user_notes_count, mr.count_of_changed_lines, mr.web_url])
    
    mr_df = pd.DataFrame(rows, columns=["TITLE", "PROJECT", "MILESTONE", "CREATED", "MERGED", "MERGED IN DAYS", "NO OF COMMENTS", "NO OF CHANGED LINES", "URL"])
    mr_df["CREATED"] = pd.to_datetime(mr_df["CREATED"])
    mr_df = mr_df.sort_values(by="CREATED")
    
    return mr_df

def create_projects_df(projects, mrs):
    rows = []
    for p in projects.values():
        count = 0
        if p.visibility != "private":
            for mr in mrs:
                if mr.project_id == p.id:
                    count += 1

            if count > 0:
                rows.append([p.name, p.web_url, count])
    
    mr_df = pd.DataFrame(rows, columns=["PROJECT", "URL", "MRs"])
    return mr_df

def create_maintainer_df(maintainer_info):
    return pd.DataFrame(maintainer_info, columns=["PROJECT", "ROLE"])