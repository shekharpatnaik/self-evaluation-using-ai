k create ns lens
kubectl delete secret lens-secret
kubectl create secret generic lens-secret \
    --from-literal=GITLAB_TOKEN=$GITLAB_TOKEN \
    --from-literal=ANTHROPIC_API_KEY=$ANTHROPIC_API_KEY \
    --from-file=$(pwd)/secrets/oauth2-proxy.cfg

kubectl apply -f k8s/