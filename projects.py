from gl import get_gl
import requests
import yaml

def get_all_projects_for_mrs(mrs):
    projects = set()
    for mr in mrs:
        projects.add(mr.project_id)

    project_data = {}
    for p in projects:
        project = get_gl().projects.get(p)
        project_data[p] = project
    
    return project_data

def get_maintainer_info(username):
    data = []
    try:
        url = f"https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/team_members/person/{username[0]}/{username}.yml?ref_type=heads"
        response = requests.get(url).content
        response_data = yaml.safe_load(response)
        for k,v in response_data['projects'].items():
            data.append({"PROJECT": k, "ROLE": v})
    except:
        print(f"Error getting maintainer info for {username}")

    return data

def remove_mrs_from_private_projects(projects, mrs):
    for mr in mrs:
        if mr.project_id in projects and projects[mr.project_id].visibility == "private":
            mrs.remove(mr)

    return list(filter(lambda mr: mr.project_id in projects and projects[mr.project_id].visibility != "private", mrs))