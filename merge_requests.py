from concurrent.futures import ThreadPoolExecutor, as_completed
from gl import get_gl
from diff import count_changed_lines

MAX_WORKERS = 10

def get_merge_requests(username, start_date, end_date, fetch_diff):
    mrs = get_gl().mergerequests.list(author_username=username, state="merged", created_after=start_date, created_before=end_date, scope="all", get_all=True)
    results = []

    with ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
        futures = []
        for mr in mrs:
            futures.append(executor.submit(fetch_diff_for_merge_requests, mr, fetch_diff))
            
        for future in as_completed(futures):
            results.append(future.result())
            
    return results

def get_changed_lines_for_mr(mr):
    count = 0
    for diff in mr.diffs:
        count += count_changed_lines(diff['diff'])

    return count

def fetch_diff_for_merge_requests(mr, fetch_diff):
    if fetch_diff:
        mr.diffs = get_gl().projects.get(mr.project_id).mergerequests.get(mr.iid).changes()['changes']
        mr.count_of_changed_lines = get_changed_lines_for_mr(mr)
    else:
        mr.diffs = []
        mr.count_of_changed_lines = 0
    return mr