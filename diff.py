def count_changed_lines(diff_content):
    # Split the content by lines
    lines = diff_content.split('\n')

    # Count the lines where the line starts with '+' or '-', but not '---' or '+++'
    changed_lines = [line for line in lines if (line.startswith('+') or line.startswith('-')) and not line.startswith('---') and not line.startswith('+++')]

    return len(changed_lines)

# # The diff string you're parsing.
# diff_str = '@@ -26,7 +26,7 @@\n       },\n       "devDependencies": {\n         "@jest/globals": "^29.7.0",\n-        "@types/jest": "^29.5.5",\n+        "@types/jest": "^29.5.6",\n         "@types/lodash": "^4.14.200",\n         "@types/node": "^13.13.52",\n         "@types/request-promise": "^4.1.49",\n@@ -2056,9 +2056,9 @@\n       }\n     },\n     "node_modules/@types/jest": {\n-      "version": "29.5.5",\n-      "resolved": "https://registry.npmjs.org/@types/jest/-/jest-29.5.5.tgz",\n-      "integrity": "sha512-ebylz2hnsWR9mYvmBFbXJXr+33UPc4+ZdxyDXh5w0FlPBTfCVN3wPL+kuOiQt3xvrK419v7XWeAs+AeOksafXg==",\n+      "version": "29.5.6",\n+      "resolved": "https://registry.npmjs.org/@types/jest/-/jest-29.5.6.tgz",\n+      "integrity": "sha512-/t9NnzkOpXb4Nfvg17ieHE6EeSjDS2SGSpNYfoLbUAeL/EOueU/RSdOWFpfQTXBEM7BguYW1XQ0EbM+6RlIh6w==",\n       "dev": true,\n       "dependencies": {\n         "expect": "^29.0.0",\n@@ -14533,9 +14533,9 @@\n       }\n     },\n     "@types/jest": {\n-      "version": "29.5.5",\n-      "resolved": "https://registry.npmjs.org/@types/jest/-/jest-29.5.5.tgz",\n-      "integrity": "sha512-ebylz2hnsWR9mYvmBFbXJXr+33UPc4+ZdxyDXh5w0FlPBTfCVN3wPL+kuOiQt3xvrK419v7XWeAs+AeOksafXg==",\n+      "version": "29.5.6",\n+      "resolved": "https://registry.npmjs.org/@types/jest/-/jest-29.5.6.tgz",\n+      "integrity": "sha512-/t9NnzkOpXb4Nfvg17ieHE6EeSjDS2SGSpNYfoLbUAeL/EOueU/RSdOWFpfQTXBEM7BguYW1XQ0EbM+6RlIh6w==",\n       "dev": true,\n       "requires": {\n         "expect": "^29.0.0",\n'

# # Count the changed lines and print the result.
# num_changed_lines = count_changed_lines(diff_str)
# print("Number of changed lines:", num_changed_lines)
