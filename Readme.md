# AI-Powered MR Achievement Log

We are piloting a new tool to help you all with your self evaluations.

How can I write my Self Evaluation more efficiently?
- Writing your talent assessment self-evaluations can be time-consuming, so we want to introduce this optional tool that we are piloting with a small number of team members to help reduce the amount of time you spend searching for your accomplishments.
- The  [AI-Powered MR Achievement Log](http://lens.shekharpatnaik.co.uk/) will use AI to summarize some of your accomplishments based on the public MRs you have authored. It won't cover everything, so you'll still need to manually highlight your achievements if there are additional achievements you would like to highlight.  You would then manually enter whichever achievements you would like into Workday
- If you opt for the tool, I hope it eases the process, however you may choose not to use this tool as well.

The tool can be accessed [here](http://lens.shekharpatnaik.co.uk/).
A demo of the tool can be seen [here](https://gitlab.enterprise.slack.com/files/UKDNTR84U/F0629TU9GQ2/ai-powered-mr-accomplishments-log.mp4).

Please leave your feedback / feature requests here.

## Running locally.

> Note: You need access to an Anthropic API key to use this tool locally

```sh
export GITLAB_TOKEN=<YOUR PAT>
export ANTHROPIC_API_KEY=<YOUR API KEY>

# Create a virtual environment
python3 -m venv venv

# Activate the venv
source ./venv/bin/activate

# Install dependencies
pip install -r requirements.txt

# Run the application
streamlit run main.py
```