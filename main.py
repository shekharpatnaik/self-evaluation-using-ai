import streamlit as st
from dataframes import create_maintainer_df, create_mr_df, create_projects_df
from gl import get_gl
from merge_requests import get_merge_requests
from projects import get_all_projects_for_mrs, get_maintainer_info, remove_mrs_from_private_projects
from stats import display_mrs_over_time, display_projects_by_mr

from summary import summarize_achievements

st.set_page_config(
    page_title="Lens",
    page_icon=":chart_with_upwards_trend:",
    layout="wide",
)

from streamlit.web.server.websocket_headers import _get_websocket_headers
headers = _get_websocket_headers()

if 'X-Forwarded-Preferred-Username' not in headers:
    st.write("Invalid login")
    st.stop()

username = headers['X-Forwarded-Preferred-Username'] 

if not(username):
    st.write("Invalid login")
    st.stop()

fetch_diff = False

with st.sidebar:
    st.title('Lens')
    st.subheader('An AI-Powered MR Achievement Log')
    st.subheader('(Public Repositories only)')
    with st.form("main"):
        # username =  st.text_input('GitLab Username', default_username).strip()
        start_date = st.date_input("Start Date")
        end_date = st.date_input("End Date")
        temperature = st.slider(
            "Temperature",
            min_value=0.0,
            max_value=1.0,
            value=0.2,
            step=0.1, help="There will be different responses each time you click Calculate because AI introduces randomness to make the text more creative and avoid repeating the same words for each request.  Temperature controls AI randomness level. Increase for randomness, decrease for predictability.")
        fetch_diff = st.toggle("Fetch changed lines", value=False)
        submitted = st.form_submit_button("Calculate", type="primary")

st.caption("This log, powered by AI, does not encompass all your MRs, given its 1500-word limitation. It's a concise selection and does not cover confidential issues or private repositories.")
st.caption("For those seeking to use this as a data point for talent assessments, it is imperative to click on the links associated with the summaries rigorously to verify their accuracy.")

if submitted:
    date_diff = (end_date - start_date).days
    if date_diff > 365:
        st.error("Date range cannot be more than 365 days")
        st.stop()

    if not get_gl().users.list(username=username, get_all=True):
        st.error("Invalid Username")
        st.stop()

    mrs = []
    projects = None
    mr_df = None
    projects_df = None

    with st.spinner('Loading MRs....'):
        mrs = get_merge_requests(username, start_date, end_date, fetch_diff)

    with st.spinner('Loading Projects....'):
        projects = get_all_projects_for_mrs(mrs)
        mrs = remove_mrs_from_private_projects(projects, mrs)

    with st.spinner('Creating Dataframes....'):
        mr_df = create_mr_df(mrs, projects)
        projects_df = create_projects_df(projects, mrs)

    list_tab, projects_tab, achievements_tab, stats_tab = st.tabs(["MR List", "Projects", "Achievements", "Graphs"])
    # list_tab, projects_tab, stats_tab = st.tabs(["MR List", "Projects", "Graphs"])

    with list_tab:
        st.dataframe(mr_df, column_config={
            "URL": st.column_config.LinkColumn("URL"),
        })
        # st.markdown(mr_df.to_html(render_links=True, justify="left"), unsafe_allow_html=True)
    
    with projects_tab:
        st.markdown("## MRs by Projects\n\n")
        # st.markdown(projects_df.to_html(render_links=True, justify="left"), unsafe_allow_html=True)
        st.dataframe(projects_df, column_config={
            "URL": st.column_config.LinkColumn("URL"),
        })
        st.markdown("## Role on Projects\n\n")
        maintainer_info = get_maintainer_info(username)
        maintainer_df = create_maintainer_df(maintainer_info)
        st.dataframe(maintainer_df)
        # st.markdown(maintainer_df.to_html(render_links=True, justify="left"), unsafe_allow_html=True)

    with stats_tab:
        display_projects_by_mr(projects_df)
        display_mrs_over_time(mr_df)

    with achievements_tab:
        st.caption(':blue[Note:] Please use this as guidance only as the LLM might hallucinate. Sometimes the LLM may not generate the complete output in such cases please regenerate.')
        progress_bar = st.progress(0, text=f"Calculating... {0}%")

        if len(mrs) > 0:
            summarize_achievements(mrs, temperature, projects, lambda x: progress_bar.progress(x, text=f"Calculating... {x}%"))
            progress_bar.empty()
