import matplotlib.pyplot as plt
import streamlit as st

def display_projects_by_mr(projects_df):
    fig, ax = plt.subplots(figsize=(2, 2))
    ax.pie(projects_df["MRs"], labels=projects_df["PROJECT"], autopct='%1.1f%%', startangle=90)
    ax.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

    st.pyplot(fig)

def display_mrs_over_time(mr_df):
    mr_df["ONES"] = 1
    mr_df["CUMULATIVE_COUNT"] = mr_df["ONES"].cumsum()

    fig, ax = plt.subplots(figsize=(6, 3))
    ax.plot(mr_df["CREATED"], mr_df["CUMULATIVE_COUNT"], marker='o', linestyle='-')
    ax.set_xlabel("Date")
    ax.set_ylabel("Cumulative MR Count")
    ax.set_title("Cumulative MRs over Time")

    st.pyplot(fig)
    mr_df.drop("ONES", axis=1, inplace=True)